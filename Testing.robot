*** Settings ***

Resource    Renode-Common.robot

*** Variables ***
${MACHINE}     @platforms/cpus/stm32f103.repl
${FIRMWARE}    @${CURDIR}/main.elf

${CRH_OFFSET}    0x04
${IDR_OFFSET}    0x08
${ODR_OFFSET}    0x0C
${True}          1
${Flase}         0

${BT_Press}      Press
${BT_Release}    Release


${LENGTH_RUN_1}     200
${CRH_GPIOC13_1}    0x44744444
${ODR_GPIOC13_1}    0x00000000
${CRH_GPIOA10_1}    0x44444444


${LENGTH_RUN_2}     1000
${LENGTH_RUN_3}     500
${ODR_GPIOC13_2}    0x00002000


${LENGTH_RUN_5}     1000
${LENGTH_RUN_6}     500
${ODR_GPIOC13_3}    0x00000000


*** Test Cases ***
TC01: Test Config GPIOC13 and GPIOA10
    [Setup]             Create Machine
    Start Simulation    
    Run Steps           ${LENGTH_RUN_1}

    ${CRH_read_reg}=              Read GpioPortC    ${CRH_OFFSET}
    ${data_return}                CMP DATA REG      ${CRH_read_reg}    ${CRH_GPIOC13_1}
    Should Be Equal As Strings    ${data_return}    ${True}

    ${ODR_read_reg}=              Read GpioPortC    ${ODR_OFFSET} 
    ${data_return}                CMP DATA REG      ${ODR_read_reg}    ${ODR_GPIOC13_1}
    Should Be Equal As Strings    ${data_return}    ${True} 

    ${CRH_read_reg}=              Read GpioPortA    ${CRH_OFFSET}
    ${data_return}                CMP DATA REG      ${CRH_read_reg}    ${CRH_GPIOA10_1}
    Should Be Equal As Strings    ${data_return}    ${True}            

    Pause Simulation
    [Teardown]          Quit Machine


TC02: Test Check Input GPIOA10 Press Button
    [Setup]             Create Machine
    Start Simulation    
    Run Steps           ${LENGTH_RUN_2}

    Set GpioPortA Button          ${BT_Press} 
    Run Steps                     ${LENGTH_RUN_3}
    ${CRH_read_reg}=              Read GpioPortC     ${ODR_OFFSET}
    ${data_return}                CMP DATA REG       ${CRH_read_reg}    ${ODR_GPIOC13_2}
    Should Be Equal As Strings    ${data_return}     ${True}
    Pause Simulation
    [Teardown]                    Quit Machine


TC03: Test Check Input GPIOA10 Release Button
    [Setup]             Create Machine
    Start Simulation    
    Run Steps           ${LENGTH_RUN_5}

    Set GpioPortA Button          ${BT_Release} 
    Run Steps                     ${LENGTH_RUN_6}
    ${CRH_read_reg}=              Read GpioPortC     ${ODR_OFFSET}
    ${data_return}                CMP DATA REG       ${CRH_read_reg}    ${ODR_GPIOC13_3}
    Should Be Equal As Strings    ${data_return}     ${True}
    Pause Simulation
    [Teardown]                    Quit Machine


#    Set GpioPortA Button    ${BT_Press} 
#    #sleep                  100milliseconds
#    Run Steps               ${LENGTH_RUN_4}
#    ${CRH_read_reg}=        Read GpioPortA     ${IDR_OFFSET} 
#    Print                   ${CRH_read_reg}    0

#    ${CRH_read_reg}=    Read GpioPortC     ${ODR_OFFSET}
#    Print               ${CRH_read_reg}    0

#    ${data_return}                CMP DATA REG      ${CRH_read_reg}    ${ODR_GPIOC13_3}
#    Should Be Equal As Strings    ${data_return}    ${True}

