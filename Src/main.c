#include "stm32f10x.h" // Device header

void delay(int rep);

int main(void)
{
	RCC->APB2ENR |= 0x14;	  /// Or  0b10000 --> Anabling Preiph GPIOC
	GPIOC->CRH &= 0xFF0FFFFF; /// Reset the PORT C PIN 13
	GPIOC->CRH |= 0x00300000; /// Set Port C PIN 13 as Output
	GPIOC->ODR |= 0x2000;	  /// Set Port C Pin 13

	GPIOA->CRH &= 0xFF0FF0FF; /// Reset the PORT C PIN 13
	GPIOA->CRH |= 0x00000800; /// Set Port C PIN 13 as Output

	while (1)
	{
		int state = ((GPIOA->IDR & (1<<10))>>10);
		if (state == 1) /// Checking status of PIN ! portA
		{
			GPIOC->ODR |= 0x2000; /// Toggle the PIN state
		}
		else
		{
			GPIOC->ODR &= ~0x2000; /// Set the PIN 13 port C high
		}
		//delay(100);
	}
}

/// Random time delay Function
void delay(int rep)
{
	for (; rep > 0; rep--)
	{
		int i;
		for (i = 0; i < 100000; i++)
		{
		}
	}
}